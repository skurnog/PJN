import codecs

from util import get_file_content


def get_word_rank():
    forms_vec = get_forms_vec()
    return create_rank("resources/potop.txt", forms_vec)


def get_forms_vec():
    forms_vec = {}
    file_content = codecs.open("resources/odm.txt", encoding='utf-8').readlines()

    for line in [line.split(",") for line in file_content]:
        basic_form = line[0].strip().lower()
        for form in line:
            forms_vec[form.strip().lower()] = basic_form
    return forms_vec


def create_rank(file_name, forms_vec):
    forms_rank = {}
    file_content = get_file_content(file_name)
    for word in file_content.split(" "):
        basic_form = forms_vec.get(word.strip())
        if basic_form is None and word != "" and not word.isdigit():
            basic_form = word.strip()
        elif basic_form is None:
            continue
        forms_rank[basic_form] = 1 if basic_form not in forms_rank.keys() else forms_rank[basic_form]+1
    sorted_list = sorted([(num, word) for word, num in forms_rank.items()], key=lambda x: x[0], reverse=True)
    return sorted_list

print(get_word_rank())
