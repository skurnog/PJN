from os import listdir
from zad1.ngrams import get_file_content, get_ngram_stat
from zad1.lang_guess import guess_lang, calculate_ngram_stats
import random


def get_random_samples(language_name, number_of_samples=50, max_sample_len=100, min_sample_len=10):
    file_list = [lang for lang in listdir("resources") if lang[:-5] == language_name]
    files_num = len(file_list)
    samples_per_file = int(number_of_samples/files_num + 7)

    samples = []
    for file_name in file_list:
        file_content = get_file_content("resources/" + file_name)
        file_len = len(file_content) - 1
        for i in range(samples_per_file):
            down_range = random.randint(0, file_len - max_sample_len - min_sample_len)
            up_range = random.randint(down_range, down_range + max_sample_len) + min_sample_len
            samples.append(file_content[down_range:up_range])
    return samples


def classify_samples(language_name, sample_set, ngram_stats, n=2):
    print("KLASYFIKUJE " + language_name)

    positives = []
    negatives = []

    for sample in sample_set:
        print(sample)
        result = guess_lang(sample, ngram_stats, n)
        if result == language_name:
            positives.append(result)
        elif result != language_name:
            negatives.append(result)
    return positives, negatives

if __name__ == '__main__':
    # print(get_random_samples("polish"))
    print(classify_samples("polish", get_random_samples("polish", number_of_samples=10), calculate_ngram_stats(10, small=True), n=10))
