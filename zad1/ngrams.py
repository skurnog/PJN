#!/usr/bin/env python
# -*- coding: iso-8859-2 -*-
import re
import codecs

PATTERN = r'^[\w ]+$'


def get_file_content(file_name):
    file_content = codecs.open(file_name, encoding='utf-8').read().replace('\n', ' ')
    file_content = filter(lambda e: re.search(PATTERN, e), file_content)
    file_content = ''.join(file_content)
    return file_content


def get_letter_ngrams(file_content, n, _ngram_vec=None):
    ngrams_vec = _ngram_vec if _ngram_vec is not None else {}
    file_content = file_content.lower().replace(' ', '')
    for i in range(len(file_content)):
        ngram = file_content[i:i + n]
        ngrams_vec[ngram] = ngrams_vec.get(ngram, 0) + 1
    return ngrams_vec


def get_word_ngrams(file_content, n, _ngram_vec=None):
    ngrams_vec = _ngram_vec if _ngram_vec is not None else {}
    file_content = file_content.lower().split()
    for i in range(len(file_content)):
        ngram = tuple(file_content[i:i + n])
        ngrams_vec[ngram] = ngrams_vec.get(ngram, 0) + 1
    return ngrams_vec


def get_ngram_stat(text, from_file=False, method='letter', n=2, ngram_vec=None):
    res = {
        'letter': get_letter_ngrams,
        'word': get_word_ngrams
    }.get(method)((get_file_content(text) if from_file else text), n, ngram_vec)

    return res
