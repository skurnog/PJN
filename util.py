import codecs
import re

PATTERN = r'^[\w ]+$'


def get_file_content(file_name):
    file_content = codecs.open(file_name, encoding='utf-8').read().replace('\n', ' ').lower()
    file_content = filter(lambda e: re.search(PATTERN, e), file_content)
    file_content = ''.join(file_content)
    return file_content
